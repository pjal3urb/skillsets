{
	"name": "Skill Sets",
	"description": "Adds additional skill sets and profiles",
	"blt_version": 2,
		"hooks": [
		{ "hook_id": "lib/tweak_data/skilltreetweakdata", "script_path": "SkillSets.lua" },
		{ "hook_id": "lib/tweak_data/moneytweakdata", "script_path": "SkillSets.lua" },
		{ "hook_id": "lib/managers/multiprofilemanager", "script_path": "SkillSets.lua" }
	]
}
