if RequiredScript == "lib/tweak_data/skilltreetweakdata" then

	local init_original = SkillTreeTweakData.init

	function SkillTreeTweakData:init(...)
		init_original(self, ...)
		
		local base_sets = #self.skill_switches
		local total_sets = base_sets + SkillSets._data.extra_sets
		
		for i = base_sets + 1, total_sets, 1 do
			table.insert(self.skill_switches, { 
				name_id = "menu_set_skill_switch_" .. i, 
				locks = { level = 100 } 
			})
		end
	end

elseif RequiredScript == "lib/tweak_data/moneytweakdata" then

	local init_original = MoneyTweakData.init

	function MoneyTweakData:init(...)
		init_original(self, ...)
		
		local base_sets = #self.skill_switch_cost
		local total_sets = base_sets + SkillSets._data.extra_sets
		
		for i = base_sets + 1, total_sets, 1 do
			table.insert(self.skill_switch_cost, { 
				spending = 0,
				offshore = 0,
			})
		end
	end
	
elseif RequiredScript == "lib/managers/multiprofilemanager" then

	--Override
	function MultiProfileManager:_check_amount()
		local wanted_amount = 15 + SkillSets._data.extra_sets
		if not self:current_profile() then
			self:save_current()
		end
		if wanted_amount < self:profile_count() then
			table.crop(self._global._profiles, wanted_amount)
			self._global._current_profile = math.min(self._global._current_profile, wanted_amount)
		elseif wanted_amount > self:profile_count() then
			local prev_current = self._global._current_profile
			self._global._current_profile = self:profile_count()
			while wanted_amount > self._global._current_profile do
				self._global._current_profile = self._global._current_profile + 1
				self:save_current()
			end
			self._global._current_profile = prev_current
		end
	end
	
end



if not _G.SkillSets then
	_G.SkillSets = {
		_path = ModPath,
		_data_path = ModPath .. "settings.txt",
		_menu_path = ModPath .. "menu.txt",
		_data = {
			extra_sets = 5,	--Default increase of profiles and skill sets
		},
	}

	function SkillSets:Save()
		local file = io.open( self._data_path, "w+" )
		if file then
			file:write( json.encode( self._data ) )
			file:close()
		end
	end

	function SkillSets:Load()
		local file = io.open( self._data_path, "r" )
		if file then
			self._data = json.decode( file:read("*all") )
			file:close()
		end
	end
	
	Hooks:Add("MenuManagerInitialize", "MenuManagerInitialize_skillset_menu", function(menu_manager)
		MenuCallbackHandler.SkillSets_set_slider_value = function(self, item)
			local value = math.round(item:value())
			item:set_value(value)
			
			if value ~= SkillSets._data.extra_sets then
				SkillSets._data.extra_sets = value
				SkillSets:Save()
			end
		end

		MenuHelper:LoadFromJsonFile( SkillSets._menu_path, SkillSets, SkillSets._data )
	end)
	
	Hooks:Add("LocalizationManagerPostInit", "LocalizationManagerPostInit_skillset_localization", function(self)
		local strings = {
			SkillSets_options_menu_title = "Skill Sets",
			SkillSets_options_menu_desc = "Options for the Skill Sets mod",
			SkillSets_extra_set_slider_title = "Extra sets",
			SkillSets_extra_set_slider_desc = "Number of extra profiles and skill sets",
		}
		
		local base_sets = #tweak_data.skilltree.skill_switches - SkillSets._data.extra_sets
		local total_sets = base_sets + SkillSets._data.extra_sets
		for i = base_sets + 1, total_sets, 1 do
			strings["menu_set_skill_switch_" .. i] = "Set #" .. i
		end
		
		self:add_localized_strings(strings)
	end)
	
	SkillSets:Load()
end
